# Unit testing in Angular with Jasmine and Karma

## Introduction
Here you can find my presentation and one code example.

## Installation 
If you want to use Karma and Jasmine in your Angular project you usually have nothing else to do after your initial project setup. These two are automatically installed as developer dependencies.

## Useful Links 
* [Jasmine](https://jasmine.github.io/)
* [Karma](https://karma-runner.github.io/6.3/intro/installation.html)
* [Jasmine Tutorial](https://codecraft.tv/courses/angular/unit-testing/jasmine-and-karma/)

