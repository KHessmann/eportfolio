import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Drink } from '../example.model';

@Injectable({
  providedIn: 'root'
})
export class ExampleService {

  mockData: string[] = [
    "Unfortunately you don't have milk at home for you're cereals.",
    "Better use your hot water for tea.",
    "Cereals with cold water, really?"];
  constructor() { }

  getMockData(drink: Drink): string{
    switch(drink){
      case Drink.MILK:
        return this.mockData[0];
      case Drink.HOT_WATER:
        return this.mockData[1]
      case Drink.COLD_WATER:
        return this.mockData[2]
    }
  }

  getMockDataAsObservable(drink: Drink): Observable<string>{
    switch(drink){
      case Drink.MILK:
        return of(this.mockData[0]);
      case Drink.HOT_WATER:
        return of(this.mockData[1]);
      case Drink.COLD_WATER:
        return of(this.mockData[2]);
    }
  }
}
