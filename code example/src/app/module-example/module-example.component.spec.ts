import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModuleExampleComponent } from './module-example.component';

describe('ModuleExampleComponent', () => {
  let component: ModuleExampleComponent;
  let fixture: ComponentFixture<ModuleExampleComponent>;

  beforeEach(
    waitForAsync(() => {
     TestBed.configureTestingModule({
      declarations: [ ModuleExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
