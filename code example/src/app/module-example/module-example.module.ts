import { NgModule } from '@angular/core';
import { AppRoutingModule } from '../app-routing.module';
import { ModuleExampleComponent } from './module-example.component';

@NgModule({
  declarations: [
    ModuleExampleComponent
  ],
  imports: [
    AppRoutingModule
  ],
  exports: [
    ModuleExampleComponent
  ],
})
export class ExampleModule { }
