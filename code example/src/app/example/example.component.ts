import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Drink } from '../example.model';
import { ExampleService } from '../services/example.service';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.scss']
})
export class ExampleComponent implements OnChanges {

  @Input()
  drink: Drink;

  data: string;
  data$: Observable<string>;

  constructor(private exampleService: ExampleService) { }

  ngOnChanges() {
    this.data = this.exampleService.getMockData(this.drink);
    this.data$ = this.exampleService.getMockDataAsObservable(this.drink);
  }

}
