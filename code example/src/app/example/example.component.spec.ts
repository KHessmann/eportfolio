import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { AnotherExampleComponent } from '../another-example/another-example.component';
import { Drink } from '../example.model';
import { ExampleModule } from '../module-example/module-example.module';
import { ExampleService } from '../services/example.service';

import { ExampleComponent } from './example.component';

describe('ExampleComponent', () => {
  let component: ExampleComponent;
  let fixture: ComponentFixture<ExampleComponent>;
  let service: ExampleService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExampleComponent, AnotherExampleComponent ],
      imports: [ExampleModule],
    })
    .compileComponents();
    service = TestBed.inject(ExampleService);
    spyOn(service, "getMockData").and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleComponent);
    component = fixture.componentInstance;
    component.drink = Drink.MILK;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have value of mockData in data ', () => {
    const expected: string = "Unfortunately you don't have milk at home for you're cereals.";
    const element: HTMLElement = fixture.nativeElement.querySelector(".text");

    component.ngOnChanges();
    fixture.detectChanges();

    expect(element.textContent).toEqual(expected);
    expect(service.getMockData).toHaveBeenCalled();
  });

  it('should change data if drink value changes', () => {
    const expectedMilk : string = "Unfortunately you don't have milk at home for you're cereals.";
    const expectedHotWater : string = "Better use your hot water for tea.";
    const expectedColdWater : string = "Cereals with cold water, really?";

    component.ngOnChanges();
    fixture.detectChanges();

    expect(component.data).toEqual(expectedMilk);

    component.drink = Drink.HOT_WATER;
    component.ngOnChanges();
    fixture.detectChanges();

    expect(component.data).toEqual(expectedHotWater);

    component.drink = Drink.COLD_WATER;
    component.ngOnChanges();
    fixture.detectChanges();

    expect(component.data).toEqual(expectedColdWater);
    expect(service.getMockData).toHaveBeenCalledTimes(3);
  });

  it('should have value of mockData in data$', () => {
    const expected: string = "Unfortunately you don't have milk at home for you're cereals.";
    const element: HTMLElement = fixture.nativeElement.querySelector(".textObservable")

    component.ngOnChanges();
    fixture.detectChanges();

    expect(element.textContent).toEqual(service.mockData[0]);
    expect(service.getMockData).toHaveBeenCalled();
  });

  it('should change data if drink value changes with Observable', () => {
    component.drink = Drink.HOT_WATER;
    const expectedHotWater : string = "Better use your hot water for tea.";
    component.ngOnChanges();
    fixture.detectChanges();

    let result$: Observable<string> = component.data$;
    result$.subscribe(
      (val)=> {
        expect(val).toEqual(expectedHotWater);
        expect(service.getMockData).toHaveBeenCalled();
      },
      (error) => {
        throw new Error("this should never be called");
      }
    )
  });
});
