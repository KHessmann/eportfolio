import { Component, OnInit } from '@angular/core';
import { Drink } from '../example.model';

@Component({
  selector: 'app-another-example',
  templateUrl: './another-example.component.html',
  styleUrls: ['./another-example.component.scss']
})
export class AnotherExampleComponent implements OnInit {
  drink: Drink = Drink.MILK

  constructor() { }

  ngOnInit() {
  }

}
