import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ExampleComponent } from '../example/example.component';
import { ExampleModule } from '../module-example/module-example.module';

import { AnotherExampleComponent } from './another-example.component';

describe('AnotherExampleComponent', () => {
  let component: AnotherExampleComponent;
  let fixture: ComponentFixture<AnotherExampleComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AnotherExampleComponent, ExampleComponent ],
      imports:[ExampleModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnotherExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
