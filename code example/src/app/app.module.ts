import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExampleComponent } from './example/example.component';
import { AnotherExampleComponent } from './another-example/another-example.component';
import { ExampleModule } from './module-example/module-example.module';

@NgModule({
  declarations: [
    AppComponent,
    ExampleComponent,
    AnotherExampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ExampleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
