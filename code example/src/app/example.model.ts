export enum Drink{
  MILK = "milk",
  COLD_WATER = "cold water",
  HOT_WATER = "hot water"
}
